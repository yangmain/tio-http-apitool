Array.prototype.ttCons = function(e) {
	var i = 0;
	for (; i < this.length && this[i] != e; i++);
	return !(i == this.length);
};

Array.prototype.indexOf = function (val) {  
    for (var i = 0; i < this.length; i++) {  
        if (this[i] == val) {  
            return i;  
        }  
    }  
    return -1;  
};  

Array.prototype.remove = function (val) {
    var index = this.indexOf(val);
    if (index > -1) {
        this.splice(index, 1);
    }
}; 

tt_rmEle = function(arr, element) {
	var index = arr.indexOf(element);  
    if (index > -1) {  
        arr.splice(index, 1);  
    } 
	return arr;
};



tt_rmEle1 = function(arr, index) {
	var temp = [];
	for (var i = 0; i < arr.length; i++) {
		if (i != index) {
			temp.push(arr[i]);
		}
	}
	return temp;
};

/**
 * 将source的属性复制给destination
 * @param {} destination
 * @param {} source
 */
talent.Util.copy = function(dest, src) {
	//destination, source
	if (src && dest) {
		for (var propertyName in src) {
			dest[propertyName] = src[propertyName];
		}
	}
};

talent.Util.setStyle = function(obj, styles) {
	if (styles) {
		for (var styleName in styles) {
			//obj.style[styleName] = styles[styleName];
			$(obj).css(styleName, styles[styleName]);
		}
	}
};

/**
 * 
 * @param {} method
 * @param {} owner
 * @param {} args
 */
talent.Util.invoke = function(method, owner, args) {
	method.apply(owner, args);
};

/**
 * 
 * @param {} formElement
 * @return {}
 */
talent.Util.formToQueryString = function(formElement) {
	return $(formElement).serialize();
};

talent.Util.toJson = function(obj) {
	return JSON.stringify(obj);
};


talent.Util.updateOption1 = function(selectElementId, isClearBeforeUpdate, item, isFilterNullValue, optionClassName,
	optionProperties, optionPropertiesFromSrc, addNull) {
	if(!item){
		return;
	}
	//{"v":"X","l":"正在加载..."}
	var items = [];
	var index = 0;
	for (var i in item){
		items[index++] = {"v":i,"l":item[i]};
	}
	var selectElement = document.getElementById(selectElementId);
	if (!selectElement){
		console.log(selectElementId);
	}
	talent.Util.updateOption(selectElement, isClearBeforeUpdate, item, "v", "l", isFilterNullValue, optionClassName, optionProperties, optionPropertiesFromSrc, addNull);
}
/**
 * 
 * @param {} selectElement
 * @param {} isClearBeforeUpdate
 * @param {} items
 * @param {} valueFieldName
 * @param {} labelFieldName
 * @param {} isFilterNullValue
 * @param {} optionClassName
 * @param {} optionProperties
 * @param {} optionPropertiesFromSrc
 */
talent.Util.updateOption = function(selectElement, isClearBeforeUpdate, items,
	valueFieldName, labelFieldName, isFilterNullValue, optionClassName,
	optionProperties, optionPropertiesFromSrc, addNull) {
	if (isClearBeforeUpdate == true) {
		selectElement.options.length = 0;
	}
	if(!items){
		return;
	}
	
	if (addNull) {
		var op1 = new Option("", "");
		selectElement.options.add(op1); 
	}
	
	var pusheddata = [];
	
	for (var data = 0; data < items.length; data++) {
		if (isFilterNullValue
				&& (items[data][valueFieldName] == "" || items[data][labelFieldName] == "")) {
			continue;
		}
		
		if (pusheddata.ttCons(items[data][valueFieldName])){
			continue;
		}
	
		var op = new Option(items[data][labelFieldName], items[data][valueFieldName]);
		op.title = items[data][labelFieldName];
		talent.Util.copy(op, optionProperties);
		selectElement.options.add(op); 
		
		if (optionClassName) {
			op.className = optionClassName;
		}
		if (optionPropertiesFromSrc) {
			for (var i = 0; i < optionPropertiesFromSrc.length; i++) {
				op[optionPropertiesFromSrc[i]] = items[data][optionPropertiesFromSrc[i]];
			}
		}
		pusheddata.push(items[data][valueFieldName]);
	}
};

/**
 * 
 * @param {} oTarget 
 * @param {} sEventType such as "click"
 * @param {} fnHandler
 */
talent.Util.addEventHandler = function(oTarget, sEventType, fnHandler) {
    if (oTarget.addEventListener) {
        oTarget.addEventListener(sEventType, fnHandler, false);   
    } else if (oTarget.attachEvent) {   
        oTarget.attachEvent("on" + sEventType, fnHandler);   
    } else {   
        oTarget["on" + sEventType] = fnHandler;   
    }
};

talent.Util.addClass = function(target, _className) {
    $(target).addClass(_className);
};

talent.Util.removeClass = function(target, name){
	$(target).removeClass(name);
};
//date t ts
/**
 * 
 * @param {} element html element
 * @param {} type ["datetime","date","d","t","ts","time","timestamp"]
 * @param {} format ["yyyy-MM-dd HH:mm:ss",...]
 */
talent.Util.setupDate = function(element, type, format){
	var ifFormat = null;     //"yyyy-MM-dd";
	if(format) {
		ifFormat = format;
	} else {
		if (type == "datetime" || type == "ts" || type == "timestamp") {
			ifFormat = "yyyy-MM-dd HH:mm:ss";
		} else if (type == "date" || type == "d") {
			ifFormat = "yyyy-MM-dd";
		} else if(type == "t" || type == "time"){
			ifFormat = "HH:mm:ss";
		}
	}
	
	var dd = function(_config)
	{
		this.doClick = function(){
			//alert(_config.el);
			WdatePicker(_config);
		};
	};
	
	element.readOnly = false;
	element.className += " Wdate";
	talent.Util.addEventHandler(element, "click", new dd({dateFmt:ifFormat, skin:'whyGreen', el:element}).doClick);
};

/**
 * 根据类名实例化js对象
 * 
 * 参考：http://rockyuse.iteye.com/blog/1426522
 * var obj = new Base();  
 * new操作符具体干了什么呢?其实很简单，就干了三件事情。
	var obj  = {}; 
	obj.__proto__ = Base.prototype;
	Base.call(obj); 
 * 
 * @param {} clazz
 * @return {}
 */
talent.Util.instanceByClass = function(clazz) {
	var str = "var instance = new " + clazz + "();";
	eval (str);
	return instance;
};

/**
 * 获取浏览器信息
 * @return {String}
 */
talent.Util.getOs = function() {
   if(navigator.userAgent.indexOf("MSIE") > 0) {
        return "ie";
   }
   if(isFirefox=navigator.userAgent.indexOf("Firefox") > 0) {
        return "firefox";
   }
   if(isSafari=navigator.userAgent.indexOf("Safari") > 0) {
        return "safari";
   }
   if(isCamino=navigator.userAgent.indexOf("Camino") > 0) {
        return "camino";
   }
   if(isMozilla=navigator.userAgent.indexOf("Gecko/") > 0) {
        return "gecko";
   }
};


talent.Util.trim = function(str, mode){
	if (!str){
		return "";
	}
	if (mode) {
		if (mode == "l"){
			return str.replace(/(^\s*)/g, ""); 
		} else if (mode == "r"){
			return str.replace(/(\s*$)/g, ""); 
		}
	}
	return str.replace(/(^\s*)|(\s*$)/g, "");
};

/**
 * 
 * @param {} _type 形如 checkbox, text等
 */
talent.Util.createInputElement = function(_type){
	var e = document.createElement("input");
		
	try{
		e.type = _type;
	} catch(e) {
		e = document.createElement("<input type='"+_type+"'>");
	}
	return e;
};












/**
 * 实例化Render
 * 
 * @param {}
 *            renderClass
 * @param {}
 *            config
 * @return {}
 */
talent.qe.instanceRender = function(renderClass, config) {
	var str = "var render = new " + renderClass + "();";
	eval (str);
	render.setConfig(config);
	return render;
};

/**
 * invoke render
 * 
 * @param {}
 *            renderClass
 * @param {}
 *            config
 */
talent.qe.invokeRender = function(renderClass, config) {
	try  {
		var render = talent.qe.instanceRender(renderClass, config);
		render.render();
	} catch (e) {
		alert("error message from talent.qe.invokeRender: render class=" + renderClass + "\r\n" + e);
	}
};

/**
 * 基础渲染者
 */
talent.qe.BaseRender = talent.Class.extend({
	/**
	 * 所有的渲染都有一个setConfig方法
	 */
	setConfig:function (config) {
		this.config = config;
	},
	init : function (){
		
	}
});

/**
 * 基础创建者
 */
talent.qe.BaseCreator = talent.Class.extend({
	/**
	 * 所有的渲染都有一个setConfig方法
	 */
	setConfig:function (config) {
		this.config = config;
	},
	getConfig:function() {
		return this.config;
	},
	init : function (){
		
	}
});


// set form start 
/**
 * usage : 
 * 1. var isArray = talent_isSpecifiedType(obj, "Array")
 * 2. var isString = talent_isSpecifiedType(obj, "String")
 * 3. var isNumber = talent_isSpecifiedType(obj, "Number")
 * 4. var isDate = talent_isSpecifiedType(obj, "Date")
 * 5. var isRegExp = talent_isSpecifiedType(obj, "RegExp")
 * 6. var isObject = talent_isSpecifiedType(obj, "Object")
 * @param {} _obj
 * @param {} type
 * @return {true if type is matching the given value, otherwise return false}
 */
function talent_isSpecifiedType(_obj, type) {
	if (arguments.length == 2){
		return Object.prototype.toString.call(_obj) == '[object '+ type + ']';
	}
	var ret = false;
	for (var i = 1; i < arguments.length; i++) {
		ret = ret || (Object.prototype.toString.call(_obj) == '[object '+ arguments[i] + ']');
		if (ret == true){
			return true;
		}
	}
	return ret;
}
//alert(talent_isSpecifiedType(new Date(),"String","Number","Date"));
/**
 * 
 * @param {} dataObj datas which will set to the html elements
 * @param {} prefix "user." "company." and so on
 * @param {} name current level'name
 * @param {} setFormReadonly true:将表单中的元素设置为只读
 */
function talent_c_setInputValueWithDataObj(dataObj, prefix, name, _form, setFormReadonly) {
	if (dataObj == null || typeof(dataObj) == "undefined") {
		return;
	}
	
	var inputElementArray = document.getElementsByName(prefix + name);

	if (!inputElementArray) {
		return;
	}
	//var isString = talent_isSpecifiedType(dataObj, "String");
	var isArray = talent_isSpecifiedType(dataObj, "Array");
	//var isNumber = talent_isSpecifiedType(dataObj, "Number");
	var isObject = talent_isSpecifiedType(dataObj, "Object");
	
	if (talent_isSpecifiedType(dataObj, "String", "Number", "Date")) {
		talent_c_setInputArrayValue(inputElementArray, dataObj, _form, setFormReadonly);
	} else if (isObject) {
		var _prefix = prefix + "";
		if (name != "") {
			_prefix += name + ".";
		}
		
		for (var i in dataObj) {
			talent_c_setInputValueWithDataObj(dataObj[i], _prefix, i, _form, setFormReadonly);
		}
	} else if (isArray) {
		if (!inputElementArray[0]) {
			return;
		}
		if (!(talent_isSpecifiedType(dataObj[0],"String", "Number", "Date"))) {
			return;
		}
		if (inputElementArray[0].tagName == "INPUT" && (inputElementArray[0].type == "checkbox" || inputElementArray[0].type == "radio")) {
			talent_c_setValueForCheckboxAndRadio(inputElementArray, dataObj, _form, setFormReadonly);
			return;
		}
		for (var i = 0; i < dataObj.length; i++) {
			talent_c_setInputValue(inputElementArray[i], dataObj[i], _form, setFormReadonly);
		}
	}
}
function talent_c_setValueForCheckboxAndRadio(inputElementArray, dataObjArray, _form, setFormReadonly) {
	for (var i = 0; i < inputElementArray.length; i++) {
		for (var j in dataObjArray) {
			if (inputElementArray[i].form != _form){
				continue;
			}
			if (dataObjArray[j] == inputElementArray[i].value) {
				inputElementArray[i].checked = true;
				if (setFormReadonly){
					$(inputElementArray[i]).attr("readonly", true);
				}
				break;
			}
		}
	}
}
/**
 * 
 * @param {} inputElement 
 * @param {} value
 */
function talent_c_setInputValue(inputElement, _value, _form, setFormReadonly) {
	if (inputElement == null) {
		return;
	}

	if (inputElement.form != _form){
		return;
	}
	
	var value = _value;
	if (_value == null || _value == "undefined") {
		value = "";
	}
	if (inputElement.tagName == "INPUT") {
	if (inputElement.type == "text" || inputElement.type == "hidden") {
		inputElement.value = value;
	} else if (inputElement.type == "checkbox" || inputElement.type == "radio") {
		inputElement.checked = (value == inputElement.value);
	}
	} else if (inputElement.tagName == "SELECT" || inputElement.tagName == "TEXTAREA") {
		inputElement.value = value;
	}
	if (setFormReadonly){
		$(inputElement).attr("readonly", true);
	}
}
function talent_c_setInputArrayValue(inputElementArray, _value, _form, setFormReadonly) {
	if (inputElementArray == null) {
		return;
	}
	for (var i = 0; i < inputElementArray.length; i++) {
		talent_c_setInputValue(inputElementArray[i], _value, _form, setFormReadonly);
	}
}

/**
 * 用表单的数据更新grid的数据
 * @param {} data
 * @param {} _form
 */
function tt_updategridContextWithForm(data, _form) {
	var es = _form.elements;
	for (var i = 0; i < es.length; i++) {
		data[es[i].name] = es[i].value;
	}
}
//set form end

/**
 * 是不是要求登陆的响应
 * @param {} responseText
 * @return {Boolean}
 */
function tt_isLoginResp(responseText) {
	if (responseText.indexOf("j_spring_security_check") > 0) {
		return true;
	}
}

/**
 * 根据ajax响应，分派
 * @param {} _data
 * @param {} contextpath
 * @return {Boolean}
 */
function tt_dispatchJsonAjax(_data, contextpath) {
	var status = _data.status;
	
	if (status >= 200 && status <= 299) { // 成功
		if (tt_isLoginResp(_data.responseText)) {
			parent.parent.parent.parent.location.href = contextpath + "/login/init.talent";
		}
		try {
			JSON.parse(_data.responseText);
		} catch(e) {
			var form = document.createElement("form");
			var input = document.createElement("input");
			
			document.body.appendChild(form);
			form.appendChild(input);
			
			form.action = contextpath + "/common/echoHtml.talent";
			form.method = "POST";
			
			input.setAttribute("name", "html");
			input.value = _data.responseText;
			
			form.submit();
			return false;
		}
		
		return true;
	} else if(status >= 300 && status <= 399) {
		if (status == 302) {
			if (tt_isLoginResp(_data.responseText)) {
				parent.parent.parent.parent.location.href = contextpath + "/login/init.talent";
			}
		}
		return false;
	} else if(status >= 400 && status <= 499) {
		if (status == 403){
			location.href = contextpath + "/common/toError/403.talent";
		}else if (status == 408){
			location.href = contextpath + "/common/toError/408.talent";
		}else{
			location.href = contextpath + "/common/toError/404.talent";
		}
	} else if(status >= 500 && status <= 599) {
		location.href = contextpath + "/common/toError/500.talent";
	}
	return false;
}

/**
 * 使表单的元素只读
 * @param {} formElement
 * @param {} makeItReadonly
 * @param {} makeItDisable
 */
function tt_disableForm(formElement, excludeIds) {
	if(formElement) {
		var es = formElement.elements;
		if(es) {
			for (var i = 0; i < es.length; i++) {
				if (excludeIds && excludeIds.ttCons(es[i].id)){
					continue;
				}
				try {
					es[i].setAttribute("readonly", true);
					es[i].setAttribute("disabled", true);
				}catch(e) {}
			}
		}
	}
}

/**
 * checkbox的自动关联，用在"select all"身上
 * @param {} _nameOfCheckbox
 * @param {} _checkboxAll
 */
var tt_autoUpdateCheckbox = function (_nameOfCheckbox, _checkboxAll) {
	var _ = function(nameOfCheckbox, checkboxAll) {
		this.h = function() {
			if (checkboxAll) {
				var checkedStatus = checkboxAll.checked;
				var checkboxs = document.getElementsByName(nameOfCheckbox);
				if (checkboxs && checkboxs.length > 0) {
					for (var i = 0; i < checkboxs.length; i++) {
						checkboxs[i].checked = checkedStatus;
					}
				}
			}
		};
	};
	
	talent.Util.addEventHandler(_checkboxAll, "change", new _(_nameOfCheckbox, _checkboxAll).h);
	talent.Util.addEventHandler(_checkboxAll, "click", new _(_nameOfCheckbox, _checkboxAll).h);
};

/**
 * 
 * checkbox的自动关联，用在"单个checkbox"身上
 * @param {} _checkbox
 * @param {} _nameOfCheckbox
 * @param {} _checkboxAll
 */
var tt_autoUpdateCheckbox1 = function (_checkbox, _nameOfCheckbox, _checkboxAll) {
	var _ = function(checkbox, nameOfCheckbox, checkboxAll) {
		this.h = function() {
			if (checkboxAll) {
				var trueC = 0;
				var falseC = 0;
				var checkboxs = document.getElementsByName(nameOfCheckbox);
				if (checkboxs && checkboxs.length > 0) {
					for (var i = 0; i < checkboxs.length; i++) {
						checkboxs[i].checked ? trueC++ : falseC++;
					}
					if (trueC > falseC){
						checkboxAll.checked = true;
					} else if (trueC == falseC){
						
					} else {
						checkboxAll.checked = false;
					}
				}
			}
		};
	};
	
	talent.Util.addEventHandler(_checkbox, "change", new _(_checkbox, _nameOfCheckbox, _checkboxAll).h);
	talent.Util.addEventHandler(_checkbox, "click", new _(_checkbox, _nameOfCheckbox, _checkboxAll).h);
};

var tt_slideUp = function(obj, slideUpObj, slideDownObj, callback) {
	this.slideUp = function() {
		$(obj).slideUp("fast");
		if (slideUpObj){
			slideUpObj.style.display = "none";
		}
		if (slideDownObj){
			slideDownObj.style.display = "";
		}
		
		if (callback){
			callback.call(slideUpObj, 1);
		}
	};
	
	this.slideDown = function() {
		$(obj).slideDown("fast");
		if (slideUpObj){
			slideUpObj.style.display = "";
		}
		if (slideDownObj){
			slideDownObj.style.display = "none";
		}
		
		if (callback){
			callback.call(slideDownObj, 2);
		}
	};
};

/**
 * 返回两个日期相关的天数
 * @param {} startDateStr 形如:2007-01-01
 * @param {} endDateStr  形如:2007-01-01
 */
function tt_twoDateDays(startDateStr, endDateStr) {
	var s1 = startDateStr.replace(/-/g, "/");
	var s2 = endDateStr.replace(/-/g, "/");
	s1 = new Date(s1);
	s2 = new Date(s2);
	
	var time= s2.getTime() - s1.getTime();
	var days = parseInt(time / (1000 * 60 * 60 * 24));
	return days;
}

var tt_ce = function(str) {
	return document.createElement(str); //节约字节
};

var tt_size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

/**
 *  1、orig = "a=&b=.&c=&d=.&e=";<br>
	new => ""<br>
	2、orig = "a=&b=bbb&c=.&d=ddd&e=";<br>
	new => "b=bbb&d=ddd&"<br>
 * @param {} initStr
 * @return {String}
 */
var tt_skipEmptyValue = function(initStr){
	if (!initStr){
		return "";
	}
	return initStr.replace(/[^&]+=\.?(?:&|$)/g, '');
};

/**
 * 随机串产生函数
 * @param {} l
 * @return {}
 */
function tt_randomChar(l) {
	var x = "poiuytrewqasdfghjklmnbvcxzQWERTYUIPLKJHGFDSAZXCVBNM";
	var tmp = "";
	for (var i = 0; i < l; i++) {
		tmp += x.charAt(Math.ceil(Math.random() * 100000000) % x.length);
	}
	return tmp;
}

function getRuntimeStyle(obj, attr) {
	if (getComputedStyle) {
		return getComputedStyle(obj, false)[attr];
	} else {
		return obj.currentStyle[attr];
	}
}


